# Example of data input
volume_cart = 250
datos = [
    {'volume': 50, 'value': 133},
    {'volume': 150, 'value': 98},
    {'volume': 150, 'value': 72},
    {'volume': 150, 'value': 100},
    {'volume': 200, 'value': 300},

]

M = list()


# Function based on Knapsack problem, and dynamic programming (bottom up)
def to_cart(volume_cart, article_list):

    """
    :param volume_cart:  Maximum capacity of the shopping car
    :param article_list: Array of dictionaries with available items [{''volume': 40, 'value': 50}, ... ]
    :return:

    """

    for x, item in enumerate(article_list):
        M.append([])
        for y in range(volume_cart + 1):
            M[x].append(0)
    for i, item in enumerate(article_list):
        for w in range(volume_cart + 1):
            if item['volume'] <= w:
                # Second bigger value
                sec = min(M[i - 1][w], item['value'] + M[i - 1][w - item['volume']])
                # Biggest value
                M[i][w] = max(M[i - 1][w], item['value'] + M[i - 1][w - item['volume']])
            else:
                M[i][w] = M[i - 1][w]
    selected_carts_value = [M[i][w], sec]
    return selected_carts_value


to_cart(volume_cart, datos)


