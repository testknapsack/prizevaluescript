# Python Script Knapsack Problem

This script is part of a test, so it returns the two highest possible values from a list of items (which have volume and value ).

Script is intended to be expanded and retrieve the data from an API, returning not only max value but which combination of items should be.

Only requirement is Python3+
